
/**
 * A Log equation, only needs one value.
 *
 */
public class Ln extends Equation {

	/**
	 * 
	 * Creates a new Equation with nothing in it
	 */
	public Ln() {
	}

	/**
	 * Constructor, takes two values like super class, but throws away second.
	 *
	 */
	public Ln(Value v1, Value v2) {
		super(v1, null);
	}

	/**
	 * Returns Log of value inside equation
	 */
	@Override
	public double getValue(double d) {
		return Math.log(v1.getValue(d));
	}

	/**
	 * Returns string representation of equation and its contents
	 */
	@Override
	public String toString() {

		return "ln(" + v1.toString() + ")";
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (o == null) {
			return false;
		}

		if (!(o instanceof Cos)) {
			return false;
		}

		Ln e = (Ln) o;

		return ((this.v1.equals(e.v1)) && (this.v2 == null) && (e.v2 == null));
	}

}
