
/**
 * Main interface for our Math package. All data classes and equation classes
 * will have a value
 *
 */
public interface Value {

	/**
	 * Returns a value when given a value Needs an argument for variables
	 */
	public double getValue(double d);

	/**
	 * Returns string representation of value and its contents
	 */
	public String toString();

	boolean equals(Object o);

}
