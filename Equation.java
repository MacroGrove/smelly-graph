
/**
 * Holds two values for use with an operation. In our program, all equations are
 * made up of two data members with an operation between them. The data members
 * must implement Value
 */

public abstract class Equation implements Value {

	protected Value v1;
	protected Value v2;
	/**
	 * 
	 * Creates a new Equation with nothing in it
	 */
	public Equation() {}

	/**
	 * Creates a new Equation with v1 and v2 as its arguments
	 */
	public Equation(Value v1, Value v2) {
		this.v1 = v1;
		this.v2 = v2;
	}
	

	public void setV(Value v1, Value v2) {
		this.v1 = v1;
		this.v2 = v2;
	}

	public Value getV1() {
		return this.v1;
	}

	public Value getV2() {
		return this.v2;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (o == null) {
			return false;
		}

		if (!(o instanceof Equation)) {
			return false;
		}

		Equation e = (Equation) o;

		return (this.v1.equals(e.v1)) && (this.v2.equals(e.v2));

	}

}
