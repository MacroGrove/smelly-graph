import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * Reads text input and turns into an Equation which we can then graph
 *
 */
public class Parser {

	private static Scanner sc;

	private static ArrayList<Value> equations;
	private static Equation e;
	private static String temp;

	/**
	 * Parses an equation from the GUI text field.
	 * 
	 * @param s - user input
	 * @return e - equation for plotting
	 */
	public static Equation parse(String s) {

		equations = new ArrayList<>();
		e = null;
		temp = s.toLowerCase();
		sc = new Scanner(temp);

		if (s.equals("")) {
			throw new NullPointerException("No equation inputted");
		}

		// Translates each pieces of the string and adds them to an ArrayList.
		while (sc.hasNext()) {
			translate(sc);
		}

		// Merges all of the components into one equation using PEMDAS.
		while (equations.size() > 1) {
			merge();
		}

		return e;
	}

	private static void translate(Scanner sc) {
		String value = sc.next();

		if (value.equals("x")) { // identifies numbers and variables

			equations.add(new Var());
			Statistics.updateStats("variable");

		} else if (Character.isDigit(value.charAt(0))) {

			equations.add(new Num(Double.valueOf(value)));
			Statistics.updateStats("number");

		} else if (value.contains("cos(") && value.contains(")")) {

			// All trig functions always receive a number or variable.

			value = value.replace("cos(", "");
			value = value.replace(")", "");

			if (Character.isAlphabetic(value.charAt(0))) {
				equations.add(e = new Cos(new Var(), null));
				Statistics.updateStats("cos()"); // Updates usage states
			} else {
				equations.add(e = new Cos(new Num(Double.valueOf(value)), null));
				Statistics.updateStats("cos()");
			}

		} else if (value.contains("sin(") && value.contains(")")) {

			value = value.replace("sin(", "");
			value = value.replace(")", "");

			if (Character.isAlphabetic(value.charAt(0))) {
				equations.add(e = new Sin(new Var(), null));
				Statistics.updateStats("sin()");
			} else {
				equations.add(e = new Sin(new Num(Double.valueOf(value)), null));
				Statistics.updateStats("sin()");
			}

		} else if (value.contains("tan(") && value.contains(")")) {

			value = value.replace("tan(", "");
			value = value.replace(")", "");

			if (Character.isAlphabetic(value.charAt(0))) {
				equations.add(e = new Tan(new Var(), null));
				Statistics.updateStats("tan()");
			} else {
				equations.add(e = new Tan(new Num(Double.valueOf(value)), null));
				Statistics.updateStats("tan()");
			}

		} else if (value.contains("sqrt(") && value.contains(")")) { // All sqrts receive all numbers and variables.

			value = value.replace("sqrt(", "");
			value = value.replace(")", "");

			if (Character.isAlphabetic(value.charAt(0))) {
				equations.add(e = new Sqrt(new Var(), null));
				Statistics.updateStats("sqrt()");
			} else {
				equations.add(e = new Sqrt(new Num(Double.valueOf(value)), null));
				Statistics.updateStats("sqrt()");
			}

		} else if (value.contains("ln(") && value.contains(")")) { // All logs receive all numbers and variables.

			value = value.replace("ln(", "");
			value = value.replace(")", "");

			if (Character.isAlphabetic(value.charAt(0))) {
				equations.add(e = new Ln(new Var(), null));
				Statistics.updateStats("ln()");

			} else {
				equations.add(e = new Ln(new Num(Double.valueOf(value)), null));
				Statistics.updateStats("ln()");
			}

		} else if (value.contains("log(") && value.contains(")")) {

			value = value.replace("log(", "");
			value = value.replace(")", "");

			if (Character.isAlphabetic(value.charAt(0))) {
				equations.add(e = new Log(new Var(), null));
				Statistics.updateStats("log()");

			} else {
				equations.add(e = new Log(new Num(Double.valueOf(value)), null));
				Statistics.updateStats("log()");
			}

		} else if (value.contains("^")) { // All other equations are set without values until merging equation

			equations.add(new Power());
			Statistics.updateStats("^");

		} else if (value.contains("*")) {

			equations.add(new Multiply());
			Statistics.updateStats("*");

		} else if (value.contains("/")) {

			equations.add(new Divide());
			Statistics.updateStats("/");

		} else if (value.contains("+")) {

			equations.add(new Plus());
			Statistics.updateStats("+");

		} else if (value.contains("-")) {

			equations.add(new Subtract());
			Statistics.updateStats("-");

		} else {
			Statistics.updateStats("Invalid");
			throw new IllegalArgumentException("Invalid equation.");
		}

		// Enables plotting of single values like x.
		if (equations.size() == 1) {
			e = new Plus(equations.get(0), new Num(0));
		}
	}

	// PEMDAS
	private static void merge() throws IllegalArgumentException {

		// Merges all power equations
		mergePower();

		// Merges all multiplication majors
		mergeMultiplication();

		// Merges all division equations
		mergeDivision();

		// Merges all addition equations
		mergeAddition();

		// Merges all subtraction equations
		mergeSubtraction();
	}

	private static void mergePower() {
		for (int i = 0; i < equations.size(); i++) {

			if (equations.get(i) instanceof Power) {
				Equation eq = (Equation) (equations.get(i));

				if (eq.getV1() == null) { // if empty operation create populated operation
					e = new Power(equations.get(i - 1), equations.get(i + 1));
					equations.set(i, e);
					equations.remove(i - 1); // removes 1st num or var
					equations.remove(i); // removes 2nd num or var
				}
			}
		}
	}

	private static void mergeMultiplication() {
		for (int i = 0; i < equations.size(); i++) {

			if (equations.get(i) instanceof Multiply) {
				Equation eq = (Equation) (equations.get(i));

				if (eq.getV1() == null) {
					e = new Multiply(equations.get(i - 1), equations.get(i + 1));
					equations.set(i, e);
					equations.remove(i - 1);
					equations.remove(i);
				}
			}
		}
	}

	private static void mergeDivision() {
		for (int i = 0; i < equations.size(); i++) {

			if (equations.get(i) instanceof Divide) {
				Equation eq = (Equation) (equations.get(i));

				if (eq.getV1() == null) {
					e = new Divide(equations.get(i - 1), equations.get(i + 1));
					equations.set(i, e);
					equations.remove(i - 1);
					equations.remove(i);
				}
			}
		}
	}

	private static void mergeAddition() {
		for (int i = 0; i < equations.size(); i++) {

			if (equations.get(i) instanceof Plus) {
				Equation eq = (Equation) (equations.get(i));

				if (eq.getV1() == null) {
					e = new Plus(equations.get(i - 1), equations.get(i + 1));
					equations.set(i, e);
					equations.remove(i - 1);
					equations.remove(i);
				}

			}

		}
	}

	private static void mergeSubtraction() {
		for (int i = 0; i < equations.size(); i++) {

			if (equations.get(i) instanceof Subtract) {
				Equation eq = (Equation) (equations.get(i));

				if (eq.getV1() == null) {

					e = new Subtract(equations.get(i - 1), equations.get(i + 1));
					equations.set(i, e);
					equations.remove(i - 1);
					equations.remove(i);
				}
			}
		}
	}

}
