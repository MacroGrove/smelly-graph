import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class ParserTest {

	private static Equation e;

	@Test
	public void plusTest() {
		Statistics.initialize();
		String s = "x + 1.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}

	@Test
	public void subtractTest() {
		Statistics.initialize();
		String s = "x - 1.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}

	@Test
	public void multiplyTest() {
		Statistics.initialize();
		String s = "x * 1.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}

	@Test
	public void divideTest() {
		Statistics.initialize();
		String s = "x / 1.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}

	@Test
	public void powerTest() {
		Statistics.initialize();
		String s = "x ^ 1.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}

	@Test
	public void cosTest() {
		Statistics.initialize();
		String s = "cos(x) + 0.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}

	@Test
	public void sinTest() {
		Statistics.initialize();
		String s = "sin(x) + 0.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}

	@Test
	public void tanTest() {
		Statistics.initialize();
		String s = "tan(x) + 0.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}

	@Test
	public void lnTest() {
		Statistics.initialize();
		String s = "ln(x) + 0.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}

	@Test
	public void logTest() {
		Statistics.initialize();
		String s = "log(x) + 0.0";
		e = Parser.parse(s);
		String r = e.toString();
		assertEquals(s, r);
	}
}
