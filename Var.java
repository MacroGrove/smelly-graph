
/**
 * 
 * Essentially a place holder for a num.
 * 
 */
public class Var implements Value {

	/**
	 * @param value for variable
	 * @return the value in the parameter.
	 */
	@Override
	public double getValue(double var) {
		return var;
	}
	/**
	 * Returns string representation of var ("x")
	 */
	@Override
	public String toString() {
		return "x";
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (o == null) {
			return false;
		}

		if (!(o instanceof Var)) {
			return false;
		}

		return true;
	}

}
