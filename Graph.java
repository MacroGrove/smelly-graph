
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

/**
 * 
 * Panel that handles all graph rendering
 *
 */
public class Graph extends JPanel {

	final int W = 640;
	final int H = 500;
	private Plot p;
	private double yzoom;
	private double xzoom;
	private Color c;
	private int colorPointer;
	
	public Graph() {
		super();
		super.setPreferredSize(new Dimension(W, H));

		p = null;
		yzoom = 0.0419;
		xzoom = 33.66;
		colorPointer = 0;

	}

	/**
	 * Draws axis and function to screen
	 */
	@Override
	public void paint(Graphics g) {
		drawAxis((Graphics2D) g);
		if (p != null)
			p.bruteForce(g, yzoom, xzoom, c);

	}
	
	
	private void drawAxis(Graphics2D g) {
		g.setStroke(new BasicStroke(2));
		g.setColor(Color.BLACK);
		g.drawLine(0, 240, 640, 240);
		g.drawLine(320, 0, 320, 480);
		
		// y
		g.drawString(((int) (240.0 * yzoom)) + "", 330, 10);
		g.drawString(((int) (-240 * yzoom)) + "", 330, 478);
		
		// x
		g.drawString(((int) (-360 / xzoom)) + "", 1, 235);
		g.drawString(((int) (360 / xzoom)) + "", 615, 235);

	}
	/**
	 * Plots given plot
	 * @param p 
	 */
	public void plot(Plot p) {
		this.p = p;
		//these values give x = �10 and y = �10
		xzoom = 33.66;
		yzoom = 0.0419;
	}
	
	/**
	 * Changes zoom by dzoom
	 * @param dzoom
	 */
	public void changeZoom(double dzoom) {
		yzoom *= dzoom;
		xzoom = (1 / yzoom) * 1.4;
	}
	/**
	 * Sets zoom to arbitrary value
	 * @param yz
	 * @param xz
	 */
	public void setZoom(double yz, double xz) {
		yzoom = yz;
		xzoom = xz;
	}
	
	/**
	 * Randomizes color of line that get graphed
	 */
	public void nextColor() {
		colorPointer++;
		if (colorPointer == 4) {
			colorPointer = 0;
		}

		switch (colorPointer) {
		case 0:
			c = Color.RED;
			break;
		case 1:
			c = Color.BLUE;
			break;
		case 2:
			c = new Color(0, 153, 0);
			// GREEN
			break;
		case 3:
			c = new Color(127, 0, 255);
			// PURPLE
			break;
		default:
			c = Color.RED;
			break;

		}

	}

}
