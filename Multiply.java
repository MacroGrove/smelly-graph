
/**
 * 
 * A Multiplication equation. Extends equation, so two objects that implement
 * Value needed.
 *
 */
public class Multiply extends Equation {
	/**
	 * Creates a new Equation with nothing in it
	 */
	public Multiply() {}
	/**
	 * Creates a new Equation with v1 and v2 as its arguments
	 */
	public Multiply(Value v1, Value v2) {
		super(v1, v2);
	}

	/**
	 * @return the value of both Values multiplied together The parameter is passed
	 *         into the Values in case there are any variable / equations with
	 *         variables
	 * 
	 */
	@Override
	public double getValue(double d) {
		return v1.getValue(d) * v2.getValue(d);
	}
	
	/**
	 * Returns string representation of equation and its contents
	 */
	@Override
	public String toString() {

		return v1.toString() + " * " + v2.toString();
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (o == null) {
			return false;
		}

		if (!(o instanceof Multiply)) {
			return false;
		}

		Multiply e = (Multiply) o;

		return ((this.v1.equals(e.v1)) && (this.v2.equals(e.v2))) || ((this.v1.equals(e.v2)) && (this.v2.equals(e.v1)));

	}
}
