
/**
 * 
 * A Power equation. Extends equation, so two objects that implement Value
 * needed.
 *
 */
public class Power extends Equation {
	/**
	 * 
	 * Creates a new Equation with nothing in it
	 */
	public Power() {}
	/**
	 * 
	 * Creates a new Equation with v1 and v2 as its arguments
	 */
	public Power(Value v1, Value v2) {
		super(v1, v2);
	}

	/**
	 * @return the value of the first value raised to the power of the second value.
	 *         The parameter is passed into the Values in case there are any
	 *         variable / equations with variables
	 * 
	 */
	@Override
	public double getValue(double d) {
		return Math.pow(v1.getValue(d), v2.getValue(d));
	}
	
	/**
	 * Returns string representation of equation and its contents
	 */
	@Override
	public String toString() {

		return v1.toString() + " ^ " + v2.toString();
	}
	
	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (o == null) {
			return false;
		}

		if (!(o instanceof Power)) {
			return false;
		}

		Power e = (Power) o;

		return ((this.v1.equals(e.v1)) && (this.v2.equals(e.v2))) || ((this.v1.equals(e.v2)) && (this.v2.equals(e.v1)));
	}
	
}
