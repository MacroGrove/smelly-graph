
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * 
 * Smelly Math's Main Window
 *
 */

public class Window extends JFrame implements KeyListener, WindowListener {

	private JPanel top;
	private JPanel bottom;
	private Graph g;
	private EquationPanel t;
	private Queue<BufferedImage> images;

	/**
	 * Creates Application
	 */
	public Window() {
		super("Smelly Math 2019");
		// changes default window icon
		super.setIconImage(
				Toolkit.getDefaultToolkit().getImage(getClass().getClassLoader().getResource("smelly_math.png")));
		top = new JPanel(); // to hold graph
		bottom = new JPanel(); // to hold equation panel
		g = new Graph();
		t = new EquationPanel();
		images = new LinkedList<>(); // holds buffered images to be written and saved
	}

	/**
	 * Displays the window with all default settings.
	 */
	public void render() {
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setLayout(new BorderLayout());

		super.addWindowListener(this); // enables window listening for JFrame
		t.addKeyListener(this); // enables keyboard listening for the JTextField

		top.add(g);
		bottom.add(t);

		super.add(top, BorderLayout.NORTH);
		super.add(bottom, BorderLayout.CENTER);

		super.pack();
		super.setLocationRelativeTo(null); // centers JFrame
		super.setVisible(true);

	}

	// Auto-corrects input where possible
	private void autoCorrect() {
		String temp = t.getText();

		if ((temp.contains("(")) && !(temp.contains(")"))) {
			t.setText(temp + ")");
		}

	}

	/**
	 * All Hotkey Events
	 */
	@Override
	public void keyPressed(KeyEvent e) {

		repaint(); // redraws the graph every time a key is written

		if (e.getKeyCode() == KeyEvent.VK_DELETE) {

			t.setText(""); // Clears text field

		} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {

			// Retrieves the text input, parses it, and plots the result.
			try {

				if (!(t.getText().trim().equals(""))) { // Tries to plot when text field contains input.
					autoCorrect();
					Equation eq = Parser.parse(t.getText());
					g.nextColor(); // Gets the color of the line.
					g.plot(new Plot(eq)); // Plots the line on the graph.

				} else {
					throw new NullPointerException("No equation inputted.");
				}

			} catch (IllegalArgumentException iea) {
				JOptionPane.showMessageDialog(this, iea.getMessage(), "Illegal Argument Exception",
						JOptionPane.ERROR_MESSAGE);
			} catch (NullPointerException npe) {
				JOptionPane.showMessageDialog(this, npe.getMessage(), "Null Pointer Exception",
						JOptionPane.ERROR_MESSAGE);
			}

		} else if (e.getKeyCode() == KeyEvent.VK_F && (e.isControlDown())) {

			// Maximizes or Normalizes Window
			if (super.getExtendedState() != JFrame.MAXIMIZED_BOTH) {
				super.setExtendedState(JFrame.MAXIMIZED_BOTH); // Maximizes window
			} else {
				super.setExtendedState(JFrame.NORMAL); // Resets window
			}

		} else if (e.getKeyCode() == KeyEvent.VK_H && (e.isControlDown())) {

			// Displays Help Window
			JOptionPane.showMessageDialog(this,
					"Equations: Polynomials, trig functions, etc.\nHotKeys: ctrl + c, delete, enter, f m, r, w, up, down",
					"Tutorial", JOptionPane.INFORMATION_MESSAGE);

		} else if (e.getKeyCode() == KeyEvent.VK_M && (e.isControlDown())) {

			// Minimizes Window
			super.setExtendedState(JFrame.ICONIFIED);

		} else if (e.getKeyCode() == KeyEvent.VK_R && (e.isControlDown())) {

			// Closes and Restarts Window
			dispose();
			Window w = new Window();
			w.render();

		} else if (e.getKeyCode() == KeyEvent.VK_C && (e.isControlDown())) {

			// setups an image
			BufferedImage image = new BufferedImage(super.getWidth(), super.getHeight(), BufferedImage.TYPE_INT_ARGB);
			super.paint(image.getGraphics()); // paints the GUI graphics onto the image
			images.add(image); // adds image to save queue

			JOptionPane.showMessageDialog(this, "Graph captured. Click ctrl+s to save your images.", "Screen Capture",
					JOptionPane.INFORMATION_MESSAGE);

		} else if (e.getKeyCode() == KeyEvent.VK_S && (e.isControlDown())) {

			// Saves Image(s)
			if (!images.isEmpty()) { // If images in image queue

				boolean isSaving = false;

				while ((!images.isEmpty()) && (!isSaving)) { // If user wants to save images

					JFileChooser j = new JFileChooser(); // Creates a File Explorer type GUI

					int option = j.showSaveDialog(this); // Shows GUI

					if (option == JFileChooser.APPROVE_OPTION) { // Saves image

						try {

							ImageIO.write(images.poll(), "png", j.getSelectedFile()); // Writes image
							JOptionPane.showMessageDialog(this, "Image saved.", "Save Image",
									JOptionPane.PLAIN_MESSAGE);

						} catch (IOException e1) {
							JOptionPane.showMessageDialog(this, "Image not saved!", "Save Image",
									JOptionPane.ERROR_MESSAGE);
						}

					} else if (option == JFileChooser.CANCEL_OPTION) { // Enables event canceling
						isSaving = true;
					}
				} // while
			} else {

				JOptionPane.showMessageDialog(this, "There are no images to save.", "Save Image",
						JOptionPane.ERROR_MESSAGE);
			}

		} else if (e.getKeyCode() == KeyEvent.VK_W && (e.isControlDown())) {

			// Ends program.
			System.exit(0);

		} else if (e.getKeyCode() == KeyEvent.VK_UP) {

			// Zoom In
			g.changeZoom(0.9);

		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {

			// Zoom Out
			g.changeZoom(1.1);

		}
	}

	@Override
	public void windowOpened(WindowEvent e) {
		JOptionPane.showMessageDialog(this,
				"Welcome to Smelly Math 2.0\nby Philip Applegate and Matthew Moody\nCopyright 2019.", "Welcome",
				JOptionPane.INFORMATION_MESSAGE);
		Statistics.initialize(); // initializes statistics class to record parsing stats
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// Not applicable
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// Not applicable
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// Not applicable

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// Not applicable
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// Not applicable
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// Not applicable
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// Not applicable
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// Not applicable
	}

}
