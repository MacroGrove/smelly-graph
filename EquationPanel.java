
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JTextField;

public class EquationPanel extends JTextField {

	final int W = 640;
	final int H = 40;
	
	Font f;

	public EquationPanel() {
		f = new Font("Calibri", Font.PLAIN, 25); // increases font size to fit text field
		super.setPreferredSize(new Dimension(W, H));
		super.setFont(f);

	}

}
