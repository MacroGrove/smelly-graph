
/**
 * A Log equation, only needs one value.
 *
 */
public class Log extends Equation {

	/**
	 * 
	 * Creates a new Equation with nothing in it
	 */
	public Log() {
	}

	/**
	 * Constructor, takes two values like super class, but throws away second.
	 *
	 */
	public Log(Value v1, Value v2) {
		super(v1, null);
	}

	/**
	 * Returns Log of value inside equation
	 */
	@Override
	public double getValue(double d) {
		return Math.log10(v1.getValue(d));
	}

	/**
	 * Returns string representation of equation and its contents
	 */
	@Override
	public String toString() {

		return "log(" + v1.toString() + ")";
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (o == null) {
			return false;
		}

		if (!(o instanceof Cos)) {
			return false;
		}

		Log e = (Log) o;

		return ((this.v1.equals(e.v1)) && (this.v2 == null) && (e.v2 == null));
	}

}
