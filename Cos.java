
/**
 * A Cosine equation, only needs one value.
 *
 */
public class Cos extends Equation {

	/**
	 * 
	 * Creates a new Equation with nothing in it
	 */
	public Cos() {}
	
	/**
	 * Constructor, takes two values like super class, but throws away second.
	 *
	 */
	public Cos(Value v1, Value v2) {
		super(v1, null);
	}

	/**
	 * Returns Cos of value inside equation
	 */
	@Override
	public double getValue(double d) {
		return Math.cos(v1.getValue(d));
	}
	
	/**
	 * Returns string representation of equation and its contents
	 */
	@Override
	public String toString() {

		return "cos(" + v1.toString() + ")";
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (o == null) {
			return false;
		}

		if (!(o instanceof Cos)) {
			return false;
		}

		Cos e = (Cos) o;

		return ((this.v1.equals(e.v1)) && (this.v2 == null) && (e.v2 == null));
	}

}
