import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class OperationsTest {

	private final int num1 = 10;
	private final int num2 = 10;

	@Test
	public void plusTest() {
		Plus e = new Plus(new Num(num1), new Num(num2));
		double r = num1 + num2;
		assertEquals(e.getValue(0), r);
	}

	@Test
	public void subtractTest() {
		Subtract e = new Subtract(new Num(num1), new Num(num2));
		double r = num1 - num2;
		assertEquals(e.getValue(0), r);
	}

	@Test
	public void multiplyTest() {
		Multiply e = new Multiply(new Num(num1), new Num(num2));
		double r = num1 * num2;
		assertEquals(e.getValue(0), r);
	}

	@Test
	public void divideTest() {
		Divide e = new Divide(new Num(num1), new Num(num2));
		double r = num1 / num2;
		assertEquals(e.getValue(0), r);
	}

	@Test
	public void powerTest() {
		Power e = new Power(new Num(num1), new Num(num2));
		double r = Math.pow(num1, num2);
		assertEquals(e.getValue(0), r);
	}

	@Test
	public void tanTest() {
		Tan n = new Tan(new Num(num1), null);
		double r = Math.tan(num1);

		assertEquals(n.getValue(0), r);
	}

	@Test
	public void sinTest() {
		Sin n = new Sin(new Num(num1), null);
		double r = Math.sin(num1);

		assertEquals(n.getValue(0), r);
	}

	@Test
	public void cosTest() {
		Cos n = new Cos(new Num(num1), null);
		double r = Math.cos(num1);

		assertEquals(n.getValue(0), r);
	}

	@Test
	public void sqrtTest() {
		Sqrt s = new Sqrt(new Num(num1), null);
		double r = Math.sqrt(num1);
		assertEquals(s.getValue(0), r);
	}

	@Test
	public void lnTest() {
		Ln l = new Ln(new Num(num1), null);
		double r = Math.log(num1);
		assertEquals(l.getValue(0), r);
	}

	@Test
	public void logTest() {
		Log l = new Log(new Num(num1), null);
		double r = Math.log10(num1);
		assertEquals(l.getValue(0), r);
	}

}
