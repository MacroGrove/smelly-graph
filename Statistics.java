import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

public class Statistics {

	private static HashMap<String, Integer> stats; // stores stats usage for each operation
	private static File f; // stores the data

	/**
	 * Initializes the HashMap and the File.
	 */
	public static void initialize() {
		stats = new HashMap<>();
		f = new File("Smelly Math Stats.txt");
		checkFile();
	}

	/**
	 * Updates the file when a new operation is used.
	 * 
	 * @param s - operation name
	 */
	public static void updateStats(String s) {
		stats.put(s, stats.get(s) + 1);
		writeToFile();
	}

	// Check if a file exists or not
	private static void checkFile() {
		if (f.exists()) {
			readFile(); // Reads file and stores is into the HashMap for updates
		} else {
			populateStats(); // Populates states with default values
			writeToFile(); // Writes to the file
		}
	}

	private static void readFile() {
		try {
			Scanner sc = new Scanner(f);
			if (sc.hasNext()) { // is not empty
				sc.nextLine(); // drops file name
				while (sc.hasNext()) {
					String s = sc.next(); // Grabs key
					int i = sc.nextInt(); // Grabs value
					stats.put(s, i); // Populates stats for updating
				}
				sc.close();
			} else { // is empty
				populateStats();
				writeToFile();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	// Writes info in stats to file
	private static void writeToFile() {
		try {
			PrintWriter pd = new PrintWriter(f);

			pd.println("Session: " + System.currentTimeMillis()); // Session Title

			for (String s : stats.keySet()) {
				pd.format(" %-10s %-5d %n", s, stats.get(s));
			}

			pd.flush(); // Ensures flushing
			pd.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	// Sets default values to write to file
	private static void populateStats() {
		stats.put("cos()", 0);
		stats.put("ln()", 0);
		stats.put("log()", 0);
		stats.put("sin()", 0);
		stats.put("tan()", 0);
		stats.put("sqrt()", 0);
		stats.put("^", 0);
		stats.put("*", 0);
		stats.put("/", 0);
		stats.put("+", 0);
		stats.put("-", 0);
		stats.put("number", 0); // Counts numbers
		stats.put("variable", 0); // Counts variables
		stats.put("Invalid", 0); // Counts errors
	}

}
