

import java.awt.Color;
import java.awt.Graphics;

/**
 * 
 *	Draws a line with a given Equation input
 *
 */

public class Plot {
	
	private Equation e;
	
	/**
	 * 
	 * @param e Plots given equation
	 */
	public Plot(Equation e) {
		this.e = e;
	}

	/**
	 * 
	 * @param g Graphics object for drawing on screen
	 * @param yzoom zoom on y axis
	 * @param xzoom zoom on x axis
	 * @param c color to draw the line
	 */
	public void bruteForce(Graphics g, double yzoom, double xzoom, Color c) {
		
		int numPts = 100;
		int[] x = new int[numPts];
		int[] y = new int[numPts];

		
		
		g.setColor(c);
		int i = -320;
		
		for(int j = 0; j < numPts; j++) {
			x[j] = (int) ((i+320));
			y[j] = (int) -(e.getValue(i/xzoom)/yzoom)+240;
			
			i += (700/(numPts));

		}
		
		g.drawPolyline(x, y, numPts);
		
		
	}
	
	/**
	 * 
	 * @return ratio to fit curve on screen where x = 320 is in top right corner
	 */
	public double curveFit() {
		return Math.abs((e.getValue(320) / 256));
	}
	


}
