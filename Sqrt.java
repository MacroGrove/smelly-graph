
/**
 * A square root number, only needs one value.
 *
 */
public class Sqrt extends Equation {

	/**
	 * 
	 * Creates a new Equation with nothing in it
	 */

	public Sqrt() {}
	/**
	 * Constructor, takes two values like super class, but throws away second.
	 *
	 */
	public Sqrt(Value v1, Value v2) {
		super(v1, null);
	}
	/**
	 * Returns Sqrt of value inside equation
	 */
	@Override
	public double getValue(double d) {
		return Math.sqrt(v1.getValue(d));
	}
	/**
	 * Returns string representation of equation and its contents
	 */
	@Override
	public String toString() {

		return "sqrt(" + v1.toString() + ")";
	}
	
	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (o == null) {
			return false;
		}

		if (!(o instanceof Sqrt)) {
			return false;
		}

		Sqrt e = (Sqrt) o;

		return ((this.v1.equals(e.v1)) && (this.v2 == null) && (e.v2 == null));
	}
}
