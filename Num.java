
/**
 * Primitive data class, holds a double.
 *
 */
public class Num implements Value {

	private double num;

	public Num(double n) {
		this.num = n;
	}

	/**
	 * Implemented method. Returns the stored double and throws away the parameter.
	 * Parameter still useful if a Num object is in an equation with a Var object.
	 * 
	 * @param var
	 * @return num
	 * 
	 */
	@Override
	public double getValue(double var) {
		return num;
	}

	/**
	 * Returns string representation of num (its value)
	 */
	@Override
	public String toString() {
		return String.valueOf(num);
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (o == null) {
			return false;
		}

		if (!(o instanceof Num)) {
			return false;
		}

		Num n = (Num) o;

		return this.num == n.num;

	}
}
